from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


import operator_benchmark as op_bench
import torch
from torch_cpu_strided_complex import cpp


"""Microbenchmarks for point-wise reduce operator."""


# Configs for pointwise reduce ops
linear_algebra_ops_configs_short = op_bench.config_list(
    attrs=[
        [512, 512],
    ],
    attr_names=['M', 'N'],
    tags=['short']
)

linear_algebra_ops_configs_long = op_bench.config_list(
    attrs=[
        [256, 256],
        [1024, 1024],
    ],
    attr_names=['M', 'N'],
    tags=['long']
)

class LinearAlgebraOpBenchmark(op_bench.TorchBenchmarkBase):
    def init(self, M, N, op_func):
        device=torch.device('cpu')
        dtype=torch.float64
        self.input_one = torch.ones((M, N), dtype=dtype, device=device)
        self.op_func = op_func

    def forward(self):
        return self.op_func(self.input_one)


linear_algebra_ops_list = op_bench.op_list(
    attr_names=['op_name', 'op_func'],
    attrs=[
        ['svd', torch.svd],
    ],
)


op_bench.generate_pt_tests_from_op_list(linear_algebra_ops_list,
                                        linear_algebra_ops_configs_short + linear_algebra_ops_configs_long,
                                        LinearAlgebraOpBenchmark)


if __name__ == "__main__":
    op_bench.benchmark_runner.main()
