import sys
import torch.cuda
from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CppExtension, CUDAExtension
from torch.utils.cpp_extension import CUDA_HOME

CXX_FLAGS = [] if sys.platform == 'win32' else ['-g', '-Werror']

ext_modules = [
    CppExtension(
        'torch_cpu_strided_complex.cpp', ['CPUComplexType.cpp'],
        extra_compile_args=CXX_FLAGS),
]

setup(
    name='torch_cpu_strided_complex',
    packages=['torch_cpu_strided_complex'],
    ext_modules=ext_modules,
    cmdclass={'build_ext': BuildExtension})


